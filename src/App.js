import React from 'react';
import './App.css';
import TabelBuah from './tugas11/tugas11'
import Timer from './tugas12/tugas12'

function App() {
  return (
    <div> 
      <TabelBuah />
      <Timer start={100}/>
    </div>
  );
}

export default App;
