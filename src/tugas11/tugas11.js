import React from 'react';

// CSS di file app.css

let dataHargaBuah = [
  {nama: "Semangka", harga: 10000, berat: 1000},
  {nama: "Anggur", harga: 40000, berat: 500},
  {nama: "Strawberry", harga: 30000, berat: 400},
  {nama: "Jeruk", harga: 30000, berat: 1000},
  {nama: "Mangga", harga: 30000, berat: 500}
]

class Welcome extends React.Component {
  render() {
    return <h1 className="headerTabelBuah">Tabel Harga Buah</h1>;
  }
}

class DataTabelBuah extends React.Component {
  render() {
    return (
      dataHargaBuah.map(x => {
        return (
          <tr className="trtdBuah">
            <td>{x.nama}</td>
            <td>{x.harga}</td>
            <td>{x.berat/1000}&nbsp;kg</td>
          </tr>
        )
      })
    );
  }
}

class TabelBuah extends React.Component {
  render() {
    return (
      <>
      <Welcome />
        <table className="tabelBuah">
          <tr className="trthBuah">
            <th className="thNamaBuah">Nama</th>
            <th className="thBuah">Harga</th>
            <th className="thBuah">Berat</th>
          </tr>
          <DataTabelBuah />
        </table>
      </>
    )
  }
}


export default TabelBuah;